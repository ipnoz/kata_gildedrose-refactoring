<?php

namespace App;

class GildedRoseTest extends \PHPUnit\Framework\TestCase {
    public function testFoo() {
        $items      = [new Item("foo", 0, 0)];
        $gildedRose = new GildedRose($items);
        $gildedRose->updateQuality();
        $this->assertEquals("foo", $items[0]->name);
    }

    /*
     * Method to compare the original output of texttest_fixture.php with
     * a new execution of the fixture.
     */
    public function testTextThirtyDays()
    {
        $thirtyDays = file_get_contents('test/data/thirtyDays.txt');
        passthru('php fixtures/texttest_fixture.php > test/output/test_output.txt');
        $test = file_get_contents('test/output/test_output.txt');

        $this->assertEquals($thirtyDays, $test);
    }
}
