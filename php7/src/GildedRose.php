<?php

namespace App;

final class GildedRose {

    private $items = [];

    public function __construct($items) {
        $this->items = $items;
    }

    public function updateQuality() {
        foreach ($this->items as $item) {
            if ('Sulfuras, Hand of Ragnaros' === $item->name) {
                continue;
            }
            $this->operateItemQuality($item);
            $this->decreaseItemSellIn($item);
            $this->operateItemQualityAfterSellIn($item);
        }
    }

    private function decreaseItemQuality(Item $item)
    {
        if ($item->quality > 0) {
            --$item->quality;
        }
    }

    private function increaseItemQuality(Item $item)
    {
        if ($item->quality < 50) {
            ++$item->quality;
        }
    }

    private function operateItemQuality(Item $item): void
    {
        switch ($item->name) {
            case 'Aged Brie':
                $this->increaseItemQuality($item);
                break;
            case 'Backstage passes to a TAFKAL80ETC concert':
                $this->increaseItemQuality($item);
                if ($item->sell_in < 11) {
                    $this->increaseItemQuality($item);
                }
                if ($item->sell_in < 6) {
                    $this->increaseItemQuality($item);
                }
                break;
            default:
                $this->decreaseItemQuality($item);
        }
    }

    private function decreaseItemSellIn(Item $item): void
    {
        --$item->sell_in;
    }

    private function operateItemQualityAfterSellIn(Item $item): void
    {
        if ($item->sell_in >= 0) {
            return;
        }
        switch ($item->name) {
            case 'Aged Brie':
                $this->increaseItemQuality($item);
                break;
            case 'Backstage passes to a TAFKAL80ETC concert':
                $item->quality = 0;
                break;
            default:
                $this->decreaseItemQuality($item);
        }
    }
}

